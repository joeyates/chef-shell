resource_name :shell_heroku
provides :shell_heroku

property :name, String, default: "heroku"

actions :install
default_action :install

action :install do
  ruby_block "install heroku-cli for #{node["os"]}" do
    block do
      case node["os"]
      when "linux"
        shell_heroku_linux
      when "darwin"
        shell_heroku_mac_os_x
      end
    end
  end
end
