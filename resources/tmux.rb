resource_name :shell_tmux
provides :shell_tmux

property :name, String, default: "tmux"

actions :install
default_action :install

action :install do
  package "tmux"
end
