resource_name :shell_user
provides :shell_user

property :username, String, name_property: true
property :home_path, String
property :shell, String, default: "/bin/bash"
property :password, [String, NilClass], default: nil
property :groups, Array, default: []

actions :create
default_action :create

action :create do
  user new_resource.username do
    action      :create
    shell       new_resource.shell
    home        new_resource.home_path
    manage_home true
    not_if      "test -d #{new_resource.home_path}"
    not_if      { new_resource.username == "root" }
  end

  user "maintain #{new_resource.username}'s shell" do
    action      :modify
    username    new_resource.username
    shell       new_resource.shell
    not_if      { node["platform"] == "mac_os_x" }
    only_if     { new_resource.shell != nil }
    only_if     "test -d #{new_resource.home_path}"
    # Is shell already set to the desired value?
    not_if      "grep '^#{new_resource.username}' /etc/passwd | grep '#{new_resource.shell}$' > /dev/null"
    not_if      { new_resource.username == "root" }
  end

  shadow = Shell::Shadow.new(username: new_resource.username)

  ruby_block "set #{new_resource.username}'s password" do
    block do
      shadow.password_hash = new_resource.password
      Shell::Shadow.update(username: new_resource.username, line: shadow.line)
    end
    not_if { new_resource.password.nil? }
    not_if { shadow.password_hash.nil? }
    not_if { shadow.password_hash == new_resource.password }
    not_if { new_resource.username == "root" }
  end

  new_resource.groups.each do |g|
    group g do
      action :modify
      members new_resource.username
      append true
    end
  end
end
