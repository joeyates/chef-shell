resource_name :shell_docker
provides :shell_docker

property :name, String, default: "docker"
property :username, String

actions :install
default_action :install

action :install do
  package "docker"
  package "docker-compose"

  group "docker" do
    action :modify
    members new_resource.username
    append true
  end
end
