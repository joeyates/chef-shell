resource_name :shell_asdf_git
provides :shell_asdf_git

property :name, String, default: "asdf"
property :user, String, required: true
property :group, String, required: true
property :home_path, [String, Proc], required: true
property :plugins, Hash, default: {}
property :asdf_revision, String, default: "v0.7.6"
property :asdf_repo_url, String, default: "https://github.com/asdf-vm/asdf.git"

actions :install
default_action :install

action :install do
  n = new_resource

  home_path =
    if n.home_path === Proc
      n.home_path.call
    else
      n.home_path
    end

  asdf_repo_path = ::File.join(home_path, ".asdf")

  git asdf_repo_path do
    action      :sync
    repository  n.asdf_repo_url
    revision    n.asdf_revision
    enable_checkout false
    user        n.user
    group       n.group
  end

  n.plugins.each do |p, versions|
    shell_asdf_git_plugin p do
      asdf_path asdf_repo_path
      user  n.user
    end

    versions.each do |v|
      shell_asdf_git_plugin_version "#{p} #{v}" do
        asdf_path asdf_repo_path
        plugin  p
        version v
        user    n.user
      end
    end

    shell_asdf_git_global p do
      asdf_path asdf_repo_path
      plugin  p
      version versions[0]
      user    n.user
      only_if { versions.any? }
    end
  end
end
