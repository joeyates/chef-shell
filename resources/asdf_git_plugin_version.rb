resource_name :shell_asdf_git_plugin_version
provides :shell_asdf_git_plugin_version

property :name, String
property :plugin, [String, Symbol], required: true
property :version, String, required: true
property :user, String, required: true
property :asdf_path, String, required: true

actions :install
default_action :install

action :install do
  asdf_plugin_version = Asdf::Git::Plugin::Version.new(
    asdf_path: new_resource.asdf_path,
    name: new_resource.plugin,
    version: new_resource.version
  )

  sudo = "sudo -u '#{new_resource.user}' -i PATH=#{new_resource.asdf_path}/bin:$PATH bash"
  asdf_script_path = "#{new_resource.asdf_path}/bin/asdf"

  bash "install asdf version #{new_resource.plugin} #{new_resource.version}" do
    code "#{sudo} '#{asdf_script_path}' install #{new_resource.plugin} #{new_resource.version} || true"
    live_stream true
    environment(
      "PATH" => "#{new_resource.asdf_path}/bin:#{ENV['PATH']}"
    )

    not_if { asdf_plugin_version.installed? }
  end
end
