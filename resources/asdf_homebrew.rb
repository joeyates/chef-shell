resource_name :shell_asdf_homebrew
provides :shell_asdf_homebrew

property :name, String, default: "asdf"
property :user, String, required: true
property :group, String, required: true
property :plugins, Hash, default: {}

actions :install
default_action :install

action :install do
  n = new_resource

  homebrew_package "asdf" do
    homebrew_user n.user
  end

  n.plugins.each do |p, versions|
    shell_asdf_homebrew_plugin p do
      user n.user
    end

    versions.each do |v|
      shell_asdf_homebrew_plugin_version "#{p} #{v}" do
        plugin  p
        version v
        user n.user
      end
    end

    shell_asdf_homebrew_global p do
      plugin  p
      version versions[0]
      user    n.user
      only_if { versions.any? }
    end
  end
end
