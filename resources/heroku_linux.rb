resource_name :shell_heroku_linux
provides :shell_heroku_linux

property :name, String, default: "heroku_linx"

actions :install
default_action :install

action :install do
  package "curl"

  heroku_cli_path = ::File.join("", "usr", "bin", "heroku")

  bash "install heroku toolbelt" do
    code "wget -qO- https://cli-assets.heroku.com/install-ubuntu.sh | sh"
    not_if { ::File.exist?(heroku_cli_path) }
  end
end
