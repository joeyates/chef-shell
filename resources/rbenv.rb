resource_name :shell_rbenv
provides :shell_rbenv

property :name, String, default: "rbenv"
property :username, String
property :group, String
property :home_path, [String, Proc]

actions :install
default_action :install

action :install do
  users_group = new_resource.group

  home_path =
    if new_resource.home_path === Proc
      new_resource.home_path.call
    else
      new_resource.home_path
    end

  rbenv_path = ::File.join(home_path, ".rbenv")

  git rbenv_path do
    repository  "https://github.com/rbenv/rbenv.git"
    revision    "master"
    enable_checkout false
    action      :sync
    enable_submodules true
    user        new_resource.username
    group       users_group
  end

  rbenv_plugins_path = ::File.join(rbenv_path, "plugins")

  directory rbenv_plugins_path do
    user        new_resource.username
    group       users_group
  end

  ruby_build_path = ::File.join(rbenv_plugins_path, "ruby-build")

  git ruby_build_path do
    repository  "https://github.com/rbenv/ruby-build.git"
    revision    "master"
    enable_checkout false
    action      :sync
    enable_submodules true
    user        new_resource.username
    group       users_group
  end

  # ruby_build dependencies

  %w(
    autoconf
    bison
  ).each do |p|
    package p
  end

  node["shell"]["rbenv"]["platform_packages"].each do |p|
    package p
  end
end
