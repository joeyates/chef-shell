resource_name :shell_asdf_homebrew_plugin
provides :shell_asdf_homebrew_plugin

property :plugin, [String, Symbol], name_property: true, required: true
property :user, String, required: true

actions :install
default_action :install

action :install do
  n = new_resource

  asdf_script_path = "/opt/homebrew/opt/asdf/libexec/bin/asdf"

  bash "install asdf plugin #{n.plugin}" do
    code "'#{asdf_script_path}' plugin add #{n.plugin} || true"
    live_stream true
    user n.user
  end
end
