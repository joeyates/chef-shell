resource_name :shell_known_hosts_entry
provides :shell_known_hosts_entry

property :name, String
property :host_info, String
property :known_hosts_path, String
property :user, String
property :group, String

actions :install
default_action :install

action :install do
  bash "add #{new_resource.name} host info to known_hosts" do
    code "echo '#{new_resource.host_info}' >> #{new_resource.known_hosts_path}"
    not_if "grep '#{new_resource.host_info}' #{new_resource.known_hosts_path}"
    user new_resource.user
    group new_resource.group
  end
end
