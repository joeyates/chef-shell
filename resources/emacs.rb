resource_name :shell_emacs
provides :shell_emacs

property :name, String, default: "emacs"

actions :install
default_action :install

action :install do
  apt_repository "emacs" do
    uri "http://ppa.launchpad.net/kelleyk/emacs/ubuntu"
    key "873503A090750CDAEB0754D93FF0E01EEAAFC9CD"
    keyserver "keyserver.ubuntu.com"
    distribution "bionic"
    components ["main"]
    not_if { node["platform"] == "mac_os_x" }
  end

  package "emacs26-nox" do
    not_if { node["platform"] == "mac_os_x" }
  end

  package "emacs" do
    only_if { node["platform"] == "mac_os_x" }
  end
end
