resource_name :shell_fwup
provides :shell_fwup

property :name, String, default: "fwup"

actions :install
default_action :install

action :install do
  fwup_cached_path = ::File.join(
    Chef::Config["file_cache_path"], "fwup_1.2.6_amd64.deb"
  )

  remote_file fwup_cached_path do
    source "https://github.com/fhunleth/fwup/releases/download/" +
      "v1.2.6/fwup_1.2.6_amd64.deb"
    mode 0644
    checksum "bba562f5a28b5f60397608dac71b741033fbdd43c388c0e2ef2eb187deff9968"
    notifies :install, "dpkg_package[fwup]", :immediately
  end

  dpkg_package "fwup" do
    source fwup_cached_path
    action :nothing
  end
end
