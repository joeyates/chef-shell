resource_name :shell_elixir
provides :shell_elixir

property :name, String, default: "elixir"
property :username, String
property :group, String
property :bin_path, [String, Proc]
property :home_path, [String, Proc]

actions :install
default_action :install

action :install do
  users_group = new_resource.group

  package "fop"

  bin_path =
    if new_resource.bin_path === Proc
      new_resource.bin_path.call
    else
      new_resource.bin_path
    end

  directory bin_path do
    mode "0755"
    owner new_resource.username
    group users_group
  end

  home_path =
    if new_resource.home_path === Proc
      new_resource.home_path.call
    else
      new_resource.home_path
    end

  kerl_script_path = ::File.join(bin_path, "kerl")

  remote_file kerl_script_path do
    source node["shell"]["elixir"]["kerl_source_url"]
    checksum node["shell"]["elixir"]["kerl_source_checksum"]
    mode "0755"
    owner new_resource.username
    group users_group
  end

  kiex_path = ::File.join(home_path, ".kiex")

  directory kiex_path do
    mode "0755"
    owner new_resource.username
    group users_group
  end

  kiex_bin_path = ::File.join(kiex_path, "bin")

  directory kiex_bin_path do
    mode "0755"
    owner new_resource.username
    group users_group
  end

  kiex_script_path = ::File.join(kiex_bin_path, "kiex")

  remote_file kiex_script_path do
    source node["shell"]["elixir"]["kiex_source_url"]
    mode "0755"
    owner new_resource.username
    group users_group
  end

  kiex_scripts_path = ::File.join(kiex_path, "scripts")

  bash "install kiex" do
    code "#{kiex_script_path} setup"
    not_if { ::File.exist?(kiex_scripts_path) }
    user new_resource.username
    group users_group
    environment(
      "HOME" => home_path
    )
  end
end
