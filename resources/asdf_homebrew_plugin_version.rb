resource_name :shell_asdf_homebrew_plugin_version
provides :shell_asdf_homebrew_plugin_version

property :name, String
property :plugin, [String, Symbol], required: true
property :version, String, required: true
property :user, String, required: true

actions :install
default_action :install

action :install do
  n = new_resource

  asdf_script_path = "/opt/homebrew/opt/asdf/libexec/bin/asdf"

  bash "install asdf #{n.plugin} version #{n.version}" do
    code "'#{asdf_script_path}' install #{n.plugin} #{n.version} || true"
    live_stream true
    user n.user
  end
end
