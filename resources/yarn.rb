resource_name :shell_yarn
provides :shell_yarn

property :name, String, default: "yarn"

actions :install
default_action :install

action :install do
  apt_repository "yarn" do
    uri "https://dl.yarnpkg.com/debian"
    key "https://dl.yarnpkg.com/debian/pubkey.gpg"
    distribution "stable"
    components ["main"]
    not_if { node["platform"] == "mac_os_x" }
  end

  package "yarn"
end
