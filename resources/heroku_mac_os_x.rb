resource_name :shell_heroku_mac_os_x
provides :shell_heroku_mac_os_x

property :name, String, default: "heroku_mac_os_x"

actions :install
default_action :install

action :install do
  homebrew_tap "heroku/brew" do
    homebrew_path "/opt/homebrew/bin/brew"
  end
  package "heroku"
end
