resource_name :shell_asdf_git_plugin
provides :shell_asdf_git_plugin

property :plugin, [String, Symbol], name_property: true, required: true
property :user, String, required: true
property :asdf_path, String, required: true

actions :install
default_action :install

action :install do
  asdf_plugin = Asdf::Git::Plugin.new(
    asdf_path: new_resource.asdf_path,
    name: new_resource.name
  )

  sudo = "sudo -u '#{new_resource.user}' -i PATH=#{new_resource.asdf_path}/bin:$PATH bash"
  asdf_script_path = "#{new_resource.asdf_path}/bin/asdf"

  ruby_block "run pre-install for asdf plugin #{new_resource.plugin}" do
    block do
      case new_resource.plugin.to_sym
      when :elixir
        package "unzip"
      when :erlang
        package "automake"
        package "autoconf"
        package "build-essential"
        package "curl"
        package "libssl-dev"
        package "libncurses5-dev"
      when :nodejs
        shell_asdf_git_nodejs_setup do
          user new_resource.user
          asdf_path new_resource.asdf_path
        end
      when :ruby
        package "libssl-dev"
      when :rust
        package "pkg-config"
      end
    end
  end

  bash "install asdf plugin #{new_resource.plugin}" do
    code "#{sudo} '#{asdf_script_path}' plugin add #{new_resource.plugin} || true"
    live_stream true

    not_if { asdf_plugin.installed? }
  end
end
