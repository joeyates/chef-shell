resource_name :shell_nvm
provides :shell_nvm

property :name, String, default: "nvm"

actions :install
default_action :install

property :username, String
property :group, String
property :home_path, [String, Proc]

action :install do
  users_group = new_resource.group

  home_path =
    if new_resource.home_path === Proc
      new_resource.home_path.call
    else
      new_resource.home_path
    end

  repo_path = ::File.join(home_path, ".nvm")

  git repo_path do
    repository  "https://github.com/creationix/nvm.git"
    revision    "master"
    enable_checkout false
    action      :sync
    enable_submodules true
    user        new_resource.username
    group       users_group
  end
end
