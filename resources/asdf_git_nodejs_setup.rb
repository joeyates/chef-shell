resource_name :shell_asdf_git_nodejs_setup
provides :shell_asdf_git_nodejs_setup

property :name, String, default: "asdf_nodejs_setup"
property :user, String, required: true
property :asdf_path, String, required: true

actions :install
default_action :install

action :install do
  sudo = "sudo -u '#{new_resource.user}' -i PATH=#{new_resource.asdf_path}/bin:$PATH bash"
  keyring_import_path = "#{new_resource.asdf_path}/plugins/nodejs/bin/import-release-team-keyring"

  bash "set up GPG keys for nodejs" do
    code "#{sudo} '#{keyring_import_path}' || true"
    live_stream true
  end
end
