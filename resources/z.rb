resource_name :shell_z
provides :shell_z

property :name, String, default: "z"
property :username, String
property :bin_path, [String, Proc]
property :group, String

actions :install
default_action :install

action :install do
  users_group = new_resource.group

  bin_path =
    if new_resource.bin_path === Proc
      new_resource.bin_path.call
    else
      new_resource.bin_path
    end

  directory bin_path do
    user new_resource.username
    group users_group
  end

  z_script_path = ::File.join(bin_path, "z")
  raw_source_url =
    "https://raw.githubusercontent.com/" +
    "rupa/z/0a47c9ceca790604df5c9a4a2bc74aba63005c21/z.sh"

  remote_file z_script_path do
    source raw_source_url
    checksum "f33f052ff4662c4f6a2f68d237409d08cd81ecdf58c2ed989068121d992bf7ce"
    mode "0755"
    owner new_resource.username
    group users_group
  end
end
