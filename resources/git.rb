resource_name :shell_git
provides :shell_git

property :name, String, default: "git"

actions :install
default_action :install

action :install do
  package "git"

  package "gitk" do
    not_if { node["platform"] == "mac_os_x" }
  end
end
