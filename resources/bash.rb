resource_name :shell_bash
provides :shell_bash

property :name, String, default: "bash"
property :user, String
property :group, String

actions :install
default_action :install

action :install do
  # Replaces Apple's bash (4.x) with Brew's version, which supports globstar
  package "bash"

  brew_bash_path = ::File.join("", "usr", "local", "bin", "bash")

  bash "add brew's bash to permitted shells" do
    code "echo #{brew_bash_path} >> /etc/shells"
    not_if "grep #{brew_bash_path} /etc/shells"
    only_if { node["platform_family"] == "mac_os_x" }
  end

  bash "set brew's bash as shell" do
    code "chsh -s #{brew_bash_path} #{new_resource.user}"
    not_if do
      user_shell = `dscl . -read /Users/#{new_resource.user} UserShell`
      current_shell = user_shell[/^UserShell: (.*)/, 1]
      current_shell == brew_bash_path
    end
    only_if { node["platform_family"] == "mac_os_x" }
  end
end
