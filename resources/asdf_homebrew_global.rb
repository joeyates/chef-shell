resource_name :shell_asdf_homebrew_global
provides :shell_asdf_homebrew_global

property :name, String
property :plugin, [String, Symbol], required: true
property :version, String, required: true
property :user, String, required: true

actions :install
default_action :install

action :install do
  n = new_resource

  asdf_script_path = "/opt/homebrew/opt/asdf/libexec/bin/asdf"

  bash "set global asdf version for #{n.plugin} to #{n.version}" do
    code "'#{asdf_script_path}' global #{n.plugin} #{n.version} || true"
    live_stream true
    user n.user
  end
end
