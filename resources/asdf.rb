resource_name :shell_asdf
provides :shell_asdf

property :name, String, default: "asdf"
property :user, String, required: true
property :group, String, required: true
property :home_path, [String, Proc], required: true
property :plugins, Hash, default: {}
property :asdf_revision, String, default: "v0.7.6"
property :asdf_repo_url, String, default: "https://github.com/asdf-vm/asdf.git"

actions :install
default_action :install

action :install do
  n = new_resource

  case node["os"]
  when "linux"
    shell_asdf_git n.name do
      user n.user
      group n.group
      home_path n.home_path
      plugins n.plugins
      asdf_revision n.asdf_revision
      asdf_repo_url n.asdf_repo_url
    end
  when "darwin"
    shell_asdf_homebrew n.name do
      user n.user
      group n.group
      plugins n.plugins
    end
  else
    raise "Unknown operating system '#{node['os']}'"
  end
end
