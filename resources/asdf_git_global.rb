resource_name :shell_asdf_git_global
provides :shell_asdf_git_global

property :name, String
property :plugin, [String, Symbol], required: true
property :version, String, required: true
property :user, String, required: true
property :asdf_path, String, required: true

actions :install
default_action :install

action :install do
  n = new_resource

  asdf_plugin_version = Asdf::Git::Plugin::Version.new(
    asdf_path: n.asdf_path,
    name: n.plugin,
    version: n.version
  )

  sudo = "sudo -u '#{n.user}' -i PATH=#{n.asdf_path}/bin:$PATH bash"
  asdf_script_path = "#{n.asdf_path}/bin/asdf"

  bash "set global asdf version for #{n.plugin} to #{n.version}" do
    code "#{sudo} '#{asdf_script_path}' global #{n.plugin} #{n.version} || true"
    live_stream true
    environment(
      "PATH" => "#{n.asdf_path}/bin:#{ENV['PATH']}"
    )

    only_if { asdf_plugin_version.installed? }
  end
end
