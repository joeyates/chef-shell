name             "shell"
maintainer       "Joe Yates"
maintainer_email "joe.g.yates@gmail.com"
license          "MIT"
description      "Installs/Configures a user's shell environment"
long_description IO.read(File.expand_path("README.md", __dir__))
version          "0.2.0"
