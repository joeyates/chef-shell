default["shell"]["packages"] = []

default["shell"]["user"] = "user"
default["shell"]["group"] = "user"
default["shell"]["shell"] = "/bin/bash"

if node["platform_family"] == "mac_os_x"
  default["shell"]["home"] = ::File.join("", "Users", node["shell"]["user"])
else
  default["shell"]["home"] = ::File.join("", "home", node["shell"]["user"])
end

default["shell"]["elixir"]["kerl_source_url"] =
  "https://raw.githubusercontent.com/" +
  "kerl/kerl/8b013051eca5b982670b9e46ed3d7457af600dd3/kerl"
default["shell"]["elixir"]["kerl_source_checksum"] =
  "500c6dd001f63a276bac62814df722fa3b1d34accd32934f6a6ee60ed36c1193"
default["shell"]["elixir"]["kiex_source_url"] =
  "https://raw.githubusercontent.com/taylor/kiex/master/kiex"

default["shell"]["rbenv"]["platform_packages"] =
  case
  when node["platform"] == "ubuntu"
    packages = %w(
      build-essential
      libffi-dev
      libgdbm-dev
      libncurses5-dev
      libreadline6-dev
      libssl-dev
      libyaml-dev
      zlib1g-dev
    )
    case
    when node["platform_version"] == "16.04"
      packages << "libgdbm3"
    when node["platform_version"] == "18.04"
      packages << "libgdbm5"
    end
    packages
  else
    []
  end
