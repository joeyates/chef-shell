module Shell; end

class Shell::Shadow
  SHADOW_FILE = "/etc/shadow".freeze

  attr_reader :username

  def self.update(username:, line:)
    updated = contents.gsub(userline_match(username), line)
    File.write(SHADOW_FILE, updated)
  end

  def self.contents
    File.read(SHADOW_FILE)
  end

  def self.userline_match(username)
    /^#{username}:.*/
  end

  def initialize(username:)
    @username = username
    @parts = nil
    @password_hash = nil
  end

  def parts
    @parts ||=
      begin
        lines = self.class.contents.split("\n")
        row = lines.grep(self.class.userline_match(self.username))[0]
        return nil if !row
        row.split(/:/)
      end
  end

  def line
    return nil if !parts
    now = parts.clone
    now[2..2] = epoch_days
    now.join(":")
  end

  def password_hash
    return nil if !parts
    parts[1]
  end

  def password_hash=(value)
    @parts ||= defaults
    @parts[1..1] = value
  end

  private def epoch_days
    Time.now.to_i / (3600 * 24)
  end
end
