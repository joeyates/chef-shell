module Asdf; end
module Asdf::Git; end
class Asdf::Git::Plugin; end

class Asdf::Git::Plugin::Version
  attr_reader :asdf_path
  attr_reader :name
  attr_reader :version

  def initialize(asdf_path:, name:, version:)
    @asdf_path = asdf_path
    @name = name
    @version = version
  end

  def installed?
    raise "Plugin #{name} not installed" if !plugin.installed?

    plugin.versions.include?(version)
  end

  private def plugin
    @plugin ||= Asdf::Git::Plugin.new(
      asdf_path: asdf_path,
      name: name
    )
  end
end
