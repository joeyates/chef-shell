module Asdf; end
module Asdf::Git; end

class Asdf::Git::Plugin
  attr_reader :asdf_path
  attr_reader :name

  def initialize(asdf_path:, name:)
    @asdf_path = asdf_path
    @name = name
  end

  def installed?
    plugins.include?(name)
  end

  def versions
    `#{asdf_bin} list #{name} 2>/dev/null`.gsub(/^\s+/, "").split("\n")
  end

  private def plugins
    `#{asdf_bin} plugin list`.split("\n")
  end

  private def asdf_bin
    File.join(asdf_path, "bin", "asdf")
  end
end
