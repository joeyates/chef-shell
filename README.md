# shell cookbook

# resources

## shell_known_hosts_entry

Extract a known hosts entry:

```shell
$ ssh-keygen -H -F <hostname>
```

# Invocation

## `recipe[shell]`

Invoking the default recipe does the following:

* sets up the user,
* configures secrets management,
* installs custom packages.

## Other recipes

* `recipe[shell:bash]` - sets up bash

# Attributes

* `node["shell"]["user"]` - the user to manage (default: "user")
* `node["shell"]["group"]` - the user's principal group (default: "user")
* `node["shell"]["shell"]` - the user's shell (default: "/bin/bash")
* `node["shell"]["home"]` - the user's home directory (default: "/home/user")
